
from setuptools import setup

config = {
        'description': 'Proof of concept for renting books in a library',
        'author': 'Gregor Štefanič',
        'url': 'https://bitbucket.org/gs63160303/visual-librarian':
        'download_url': 'https://bitbucket.org/gs63160303/visual-librarian',
        'author_email': 'gs4267@student.uni-lj.si',
        'version': '0.0.1',
        'install_requires': [],
        'packages': ['visual-librarian'],
        'scripts': [],
        'name': 'visual-librarian'
}

# Add in any extra build steps for cython, etc.

setup(**config)
